Repository with all final part of the development of the thesis for classify land covers with Corine Land Cover with SAR images using CNN1
1. The "histories" folder contains the history of the two models that obtained the best results.
2. The "models" folder contains the weights of the two best-performing models, which can be imported for use.
3. There is a zip with the images that were used for the training, validation and test of the model.
4. The file "model_metrics.csv" contains the metrics of all the models that were generated.
5. There are two notebooks, which contain the entire process, as well as the code that was made for the development of the project.
6. The file "tif2np.py" is a script that allows to transform images in .tif format into numpy arrays.
